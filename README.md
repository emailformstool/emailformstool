## Online email forms tool

###  We can do powerful email form plugin for your website

Looking for email form tool? We helps you to extend the functionality and build multi-page forms, email subscription forms, payment forms, order forms, use smart conditional logic, and more.

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

### You can send/get mails to many email addresseses

We helps to quickly create beautiful email forms and comes with all the goodies by our [email forms tool](https://formtitan.com)

Happy email forms tool!